﻿namespace IS_lab_3
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.numericKey = new System.Windows.Forms.NumericUpDown();
            this.decrypt1 = new System.Windows.Forms.Button();
            this.encrypt1 = new System.Windows.Forms.Button();
            this.fileLabel1 = new System.Windows.Forms.Label();
            this.chooseFile1 = new System.Windows.Forms.Button();
            this.useFile1 = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1o = new System.Windows.Forms.TextBox();
            this.textBox1i = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.create_table = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textKey = new System.Windows.Forms.TextBox();
            this.decrypt2 = new System.Windows.Forms.Button();
            this.encrypt2 = new System.Windows.Forms.Button();
            this.fileLabel2 = new System.Windows.Forms.Label();
            this.chooseFile2 = new System.Windows.Forms.Button();
            this.useFile2 = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox2o = new System.Windows.Forms.TextBox();
            this.textBox2i = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericKey)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(-2, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(348, 241);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.numericKey);
            this.tabPage1.Controls.Add(this.decrypt1);
            this.tabPage1.Controls.Add(this.encrypt1);
            this.tabPage1.Controls.Add(this.fileLabel1);
            this.tabPage1.Controls.Add(this.chooseFile1);
            this.tabPage1.Controls.Add(this.useFile1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.textBox1o);
            this.tabPage1.Controls.Add(this.textBox1i);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(340, 215);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Шифр Цезаря";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(183, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Ключ";
            // 
            // numericKey
            // 
            this.numericKey.Location = new System.Drawing.Point(225, 36);
            this.numericKey.Name = "numericKey";
            this.numericKey.Size = new System.Drawing.Size(109, 20);
            this.numericKey.TabIndex = 17;
            // 
            // decrypt1
            // 
            this.decrypt1.Location = new System.Drawing.Point(219, 187);
            this.decrypt1.Name = "decrypt1";
            this.decrypt1.Size = new System.Drawing.Size(115, 23);
            this.decrypt1.TabIndex = 16;
            this.decrypt1.Text = "Дешифровать";
            this.decrypt1.UseVisualStyleBackColor = true;
            this.decrypt1.Click += new System.EventHandler(this.decrypt1_Click);
            // 
            // encrypt1
            // 
            this.encrypt1.Location = new System.Drawing.Point(6, 188);
            this.encrypt1.Name = "encrypt1";
            this.encrypt1.Size = new System.Drawing.Size(115, 23);
            this.encrypt1.TabIndex = 15;
            this.encrypt1.Text = "Зашифровать";
            this.encrypt1.UseVisualStyleBackColor = true;
            this.encrypt1.Click += new System.EventHandler(this.encrypt1_Click);
            // 
            // fileLabel1
            // 
            this.fileLabel1.AutoSize = true;
            this.fileLabel1.Location = new System.Drawing.Point(183, 11);
            this.fileLabel1.Name = "fileLabel1";
            this.fileLabel1.Size = new System.Drawing.Size(36, 13);
            this.fileLabel1.TabIndex = 6;
            this.fileLabel1.Text = "Файл";
            this.fileLabel1.Visible = false;
            // 
            // chooseFile1
            // 
            this.chooseFile1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chooseFile1.Location = new System.Drawing.Point(225, 9);
            this.chooseFile1.Name = "chooseFile1";
            this.chooseFile1.Size = new System.Drawing.Size(109, 18);
            this.chooseFile1.TabIndex = 5;
            this.chooseFile1.Text = "...";
            this.chooseFile1.UseVisualStyleBackColor = true;
            this.chooseFile1.Visible = false;
            this.chooseFile1.Click += new System.EventHandler(this.chooseFile1_Click);
            // 
            // useFile1
            // 
            this.useFile1.AutoSize = true;
            this.useFile1.Location = new System.Drawing.Point(6, 9);
            this.useFile1.Name = "useFile1";
            this.useFile1.Size = new System.Drawing.Size(128, 17);
            this.useFile1.TabIndex = 4;
            this.useFile1.Text = "Использовать файл";
            this.useFile1.UseVisualStyleBackColor = true;
            this.useFile1.CheckedChanged += new System.EventHandler(this.useFile1_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Результат";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Исходный текст";
            // 
            // textBox1o
            // 
            this.textBox1o.Location = new System.Drawing.Point(6, 132);
            this.textBox1o.Multiline = true;
            this.textBox1o.Name = "textBox1o";
            this.textBox1o.Size = new System.Drawing.Size(328, 50);
            this.textBox1o.TabIndex = 1;
            // 
            // textBox1i
            // 
            this.textBox1i.Location = new System.Drawing.Point(6, 63);
            this.textBox1i.Multiline = true;
            this.textBox1i.Name = "textBox1i";
            this.textBox1i.Size = new System.Drawing.Size(328, 50);
            this.textBox1i.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.create_table);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.textKey);
            this.tabPage2.Controls.Add(this.decrypt2);
            this.tabPage2.Controls.Add(this.encrypt2);
            this.tabPage2.Controls.Add(this.fileLabel2);
            this.tabPage2.Controls.Add(this.chooseFile2);
            this.tabPage2.Controls.Add(this.useFile2);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.textBox2o);
            this.tabPage2.Controls.Add(this.textBox2i);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(340, 215);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Шифр Вижинера";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // create_table
            // 
            this.create_table.Location = new System.Drawing.Point(122, 188);
            this.create_table.Name = "create_table";
            this.create_table.Size = new System.Drawing.Size(90, 23);
            this.create_table.TabIndex = 17;
            this.create_table.Text = "Таблица";
            this.create_table.UseVisualStyleBackColor = true;
            this.create_table.Click += new System.EventHandler(this.create_table_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(183, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Ключ";
            // 
            // textKey
            // 
            this.textKey.Location = new System.Drawing.Point(225, 36);
            this.textKey.Name = "textKey";
            this.textKey.Size = new System.Drawing.Size(109, 20);
            this.textKey.TabIndex = 15;
            // 
            // decrypt2
            // 
            this.decrypt2.Location = new System.Drawing.Point(238, 188);
            this.decrypt2.Name = "decrypt2";
            this.decrypt2.Size = new System.Drawing.Size(96, 23);
            this.decrypt2.TabIndex = 14;
            this.decrypt2.Text = "Дешифровать";
            this.decrypt2.UseVisualStyleBackColor = true;
            this.decrypt2.Click += new System.EventHandler(this.decrypt2_Click);
            // 
            // encrypt2
            // 
            this.encrypt2.Location = new System.Drawing.Point(6, 188);
            this.encrypt2.Name = "encrypt2";
            this.encrypt2.Size = new System.Drawing.Size(90, 23);
            this.encrypt2.TabIndex = 13;
            this.encrypt2.Text = "Зашифровать";
            this.encrypt2.UseVisualStyleBackColor = true;
            this.encrypt2.Click += new System.EventHandler(this.encrypt2_Click);
            // 
            // fileLabel2
            // 
            this.fileLabel2.AutoSize = true;
            this.fileLabel2.Location = new System.Drawing.Point(183, 11);
            this.fileLabel2.Name = "fileLabel2";
            this.fileLabel2.Size = new System.Drawing.Size(36, 13);
            this.fileLabel2.TabIndex = 12;
            this.fileLabel2.Text = "Файл";
            this.fileLabel2.Visible = false;
            // 
            // chooseFile2
            // 
            this.chooseFile2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chooseFile2.Location = new System.Drawing.Point(225, 9);
            this.chooseFile2.Name = "chooseFile2";
            this.chooseFile2.Size = new System.Drawing.Size(109, 18);
            this.chooseFile2.TabIndex = 11;
            this.chooseFile2.Text = "...";
            this.chooseFile2.UseVisualStyleBackColor = true;
            this.chooseFile2.Visible = false;
            this.chooseFile2.Click += new System.EventHandler(this.chooseFile2_Click);
            // 
            // useFile2
            // 
            this.useFile2.AutoSize = true;
            this.useFile2.Location = new System.Drawing.Point(6, 9);
            this.useFile2.Name = "useFile2";
            this.useFile2.Size = new System.Drawing.Size(128, 17);
            this.useFile2.TabIndex = 10;
            this.useFile2.Text = "Использовать файл";
            this.useFile2.UseVisualStyleBackColor = true;
            this.useFile2.CheckedChanged += new System.EventHandler(this.useFile2_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Результат";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Исходный текст";
            // 
            // textBox2o
            // 
            this.textBox2o.Location = new System.Drawing.Point(6, 132);
            this.textBox2o.Multiline = true;
            this.textBox2o.Name = "textBox2o";
            this.textBox2o.Size = new System.Drawing.Size(328, 50);
            this.textBox2o.TabIndex = 7;
            // 
            // textBox2i
            // 
            this.textBox2i.Location = new System.Drawing.Point(6, 63);
            this.textBox2i.Multiline = true;
            this.textBox2i.Name = "textBox2i";
            this.textBox2i.Size = new System.Drawing.Size(328, 50);
            this.textBox2i.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 240);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Шифры простой замены";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericKey)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label fileLabel1;
        private System.Windows.Forms.Button chooseFile1;
        private System.Windows.Forms.CheckBox useFile1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1o;
        private System.Windows.Forms.TextBox textBox1i;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label fileLabel2;
        private System.Windows.Forms.Button chooseFile2;
        private System.Windows.Forms.CheckBox useFile2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox2o;
        private System.Windows.Forms.TextBox textBox2i;
        private System.Windows.Forms.Button decrypt2;
        private System.Windows.Forms.Button encrypt2;
        private System.Windows.Forms.Button decrypt1;
        private System.Windows.Forms.Button encrypt1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericKey;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textKey;
        private System.Windows.Forms.Button create_table;
    }
}

