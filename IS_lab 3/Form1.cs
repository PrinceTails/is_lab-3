﻿using System;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace IS_lab_3
{
    public partial class Form1 : Form
    {
        StreamReader sr;
        public Form1()
        {
            InitializeComponent();

        }

        private void useFile1_CheckedChanged(object sender, EventArgs e)
        {
            if(useFile1.Checked)
            {
                fileLabel1.Visible = true;
                chooseFile1.Visible = true;
                textBox1i.Enabled = false;
            }
            else
            {
                fileLabel1.Visible = false;
                chooseFile1.Visible = false;
                textBox1i.Enabled = true;
            }
        }

        private void useFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (useFile2.Checked)
            {
                fileLabel2.Visible = true;
                chooseFile2.Visible = true;
                textBox2i.Enabled = false;
            }
            else
            {
                fileLabel2.Visible = false;
                chooseFile2.Visible = false;
                textBox2i.Enabled = true;
            }
        }

        private void chooseFile1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if(ofd.ShowDialog()==DialogResult.OK)
            {
                chooseFile1.Text = ofd.SafeFileName;
                Stream stream = ofd.OpenFile();
                sr = new StreamReader(stream,Encoding.Default);
            }
        }

        private void encrypt1_Click(object sender, EventArgs e)
        {
            if(useFile1.Checked)
            {
                if (chooseFile1.Text == "...")
                {
                    MessageBox.Show("Выберите файл.", "Не выбран файл!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string content = sr.ReadToEnd();
                textBox1o.Text = Cesar(content, Convert.ToInt32(numericKey.Value));
                sr.Close();
            }
            else
                textBox1o.Text = Cesar(textBox1i.Text, Convert.ToInt32(numericKey.Value));
        }

        private string Cesar(string input, int key)
        {
            char[] array = input.ToCharArray();
            for (int i = 0; i < array.GetLength(0); i++)
            {
                array[i] = Convert.ToChar(array[i] + key);
            }
            return new string(array);
        }

        private void decrypt1_Click(object sender, EventArgs e)
        {
            if (useFile1.Checked)
            {
                if (chooseFile1.Text == "...")
                {
                    MessageBox.Show("Выберите файл.", "Не выбран файл!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string content = sr.ReadToEnd();
                textBox1o.Text = Cesar(content, -Convert.ToInt32(numericKey.Value));
                sr.Close();
            }
            else
                textBox1o.Text = Cesar(textBox1i.Text, -Convert.ToInt32(numericKey.Value));
        }

        private void encrypt2_Click(object sender, EventArgs e)
        {
            if (useFile2.Checked)
            {
                if (chooseFile2.Text == "...")
                {
                    MessageBox.Show("Выберите файл.", "Не выбран файл!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string content = sr.ReadToEnd();
                textBox2o.Text = Vigenere(content, textKey.Text,true);
                sr.Close();
            }
            else
                textBox2o.Text = Vigenere(textBox2i.Text, textKey.Text, true);
        }
        private string Vigenere(string text,string key, bool encrypt)
        {
            bool flag = encrypt;
            char[] keyChar = key.ToCharArray();
            char[] array = text.ToCharArray();

            for (int i = 0, j=0; i < array.GetLength(0); i++,j++)
            {
                if(flag)
                    array[i] = Convert.ToChar(array[i] + (int)keyChar[j]);
                else
                    array[i] = Convert.ToChar(array[i] - (int)keyChar[j]);

                if (j == keyChar.GetLength(0) - 1)
                    j = -1;
            }

            return new string(array);
        }

        private void decrypt2_Click(object sender, EventArgs e)
        {
            if (useFile2.Checked)
            {
                if (chooseFile2.Text == "...")
                {
                    MessageBox.Show("Выберите файл.", "Не выбран файл!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string content = sr.ReadToEnd();
                textBox2o.Text = Vigenere(content, textKey.Text, false);
                sr.Close();
            }
            else
                textBox2o.Text = Vigenere(textBox2i.Text, textKey.Text, false);
        }

        private void create_table_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.DefaultExt = "txt";
            sfd.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (sfd.ShowDialog()==DialogResult.OK)
            {
                string alphabet = "abcdefghijklmnopqrstuvwxyz";
                StreamWriter sw = new StreamWriter(sfd.OpenFile(), Encoding.Default);
                var array = alphabet.ToCharArray();
                for (int i=0;i<array.GetLength(0);i++)
                {
                    sw.WriteLine(array[i] + " - " + (int)array[i]);
                }
                sw.Close();
            }
        }

        private void chooseFile2_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                chooseFile2.Text = ofd.SafeFileName;
                Stream stream = ofd.OpenFile();
                sr = new StreamReader(stream, Encoding.Default);
            }
        }
    }
}
